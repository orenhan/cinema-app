package com.example.cinema_app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.cinema_app.databinding.FragmentAccountBinding;
import com.example.cinema_app.model.FirebaseModel;
import com.example.cinema_app.model.Model;
import com.example.cinema_app.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class AccountFragment extends Fragment {
    FragmentAccountBinding binding;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private Boolean isImageSelected = false;

    public AccountFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.accountImg.setImageBitmap(result);
                    isImageSelected = true;
                }
            }
        });

        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null){
                    binding.accountImg.setImageURI(result);
                    isImageSelected = true;
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAccountBinding.inflate(inflater,container,false);
        View view = binding.getRoot();

        setFields();
        setSaveButtonActionListener();
        setLogoutButtonActionListener();
        setCameraButtonActionListener();
        setGalleryButtonActionListener();

        return view;
    }

    private void setGalleryButtonActionListener() {
        binding.galleryButton.setOnClickListener(view1->{
            galleryLauncher.launch("image/*");
        });
    }

    private void setCameraButtonActionListener() {
        binding.cameraButton.setOnClickListener(view1->{
            cameraLauncher.launch(null);
        });
    }

    private void setFields() {
        FirebaseModel firebaseModel = new FirebaseModel();
        firebaseModel.getUserById(FirebaseAuth.getInstance().getCurrentUser().getUid(), user -> {
            binding.nameEt.setText(user.getFullName());
            binding.passwordEt.setText(user.getPassword());
            binding.phoneEt.setText(user.getPhoneNumber());
            binding.addressEt.setText(user.getAddress());

            if (!Objects.equals(user.getImageUrl(), ""))
            {
                Picasso.get().load(user.getImageUrl()).into(binding.accountImg);
            }
        });
    }

    private void setSaveButtonActionListener() {
        binding.saveBtn.setOnClickListener(view1 -> {
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            String fullName = binding.nameEt.getText().toString();
            String password = binding.passwordEt.getText().toString();
            String phoneNumber = binding.phoneEt.getText().toString();
            String address = binding.addressEt.getText().toString();

            User user = new User(userId, fullName, password, phoneNumber, address, "");

            if (isImageSelected) {
                binding.accountImg.setDrawingCacheEnabled(true);
                binding.accountImg.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) binding.accountImg.getDrawable()).getBitmap();
                Model.instance().uploadImage(userId, bitmap, url->{
                    if (url != null){
                        user.setImageUrl(url);
                    }
                    Model.instance().addUser(user, (unused) -> {
                        Navigation.findNavController(view1).popBackStack();
                    });
                });
            } else {
                Model.instance().addUser(user, (unused) -> {
                    Navigation.findNavController(view1).popBackStack();
                });
            }
        });
    }

    private void setLogoutButtonActionListener() {
        binding.logoutBtn.setOnClickListener(view1 -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        });
    }
}