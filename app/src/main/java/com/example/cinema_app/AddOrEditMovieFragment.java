package com.example.cinema_app;

import static java.lang.Integer.parseInt;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.MenuProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.Navigation;

import com.example.cinema_app.databinding.FragmentAddOrEditMovieBinding;
import com.example.cinema_app.model.Model;
import com.example.cinema_app.model.Movie;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.Objects;

public class AddOrEditMovieFragment extends Fragment {
    private FragmentAddOrEditMovieBinding binding;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private Boolean isImageSelected = false;

    public AddOrEditMovieFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentActivity parentActivity = getActivity();

        parentActivity.addMenuProvider(new MenuProvider() {
            @Override
            public void onCreateMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater) {
                menu.removeItem(R.id.AddOrEditMovieFragment);
            }

            @Override
            public boolean onMenuItemSelected(@NonNull MenuItem menuItem) {
                return false;
            }
        },this, Lifecycle.State.RESUMED);

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.movieImg.setImageBitmap(result);
                    isImageSelected = true;
                }
            }
        });

        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null){
                    binding.movieImg.setImageURI(result);
                    isImageSelected = true;
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAddOrEditMovieBinding.inflate(inflater,container,false);
        View view = binding.getRoot();

        setFields();
        setSaveButtonActionListener();
        setCancelButtonActionListener();
        setCameraButtonActionListener();
        setGalleryButtonActionListener();

        return view;
    }

    private void setFields() {
        Bundle bundle = getArguments();

        if (bundle.get("movie") != null) {
            Movie movie = (Movie) bundle.get("movie");
            binding.nameEt.setText(movie.getName());
            binding.authorEt.setText(movie.getAuthor());
            binding.categoryEt.setText(movie.getCategory());
            binding.rateEt.setText("" + movie.getRate() + "");

            if (!Objects.equals(movie.getImageUrl(), ""))
            {
                Picasso.get().load(movie.getImageUrl()).into(binding.movieImg);
            }
        }
    }

    private void setGalleryButtonActionListener() {
        binding.galleryButton.setOnClickListener(view1->{
            galleryLauncher.launch("image/*");
        });
    }

    private void setCameraButtonActionListener() {
        binding.cameraButton.setOnClickListener(view1->{
            cameraLauncher.launch(null);
        });
    }

    private void setCancelButtonActionListener() {
        binding.cancelBtn.setOnClickListener(view1 -> Navigation.findNavController(view1).popBackStack(R.id.myPostsFragment,false));
    }

    private void setSaveButtonActionListener() {
        binding.saveBtn.setOnClickListener(view1 -> {
            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.custom_dialog);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();

            String name = binding.nameEt.getText().toString();
            String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
            String author = binding.authorEt.getText().toString();
            String category = binding.categoryEt.getText().toString();
            String review = binding.reviewEt.getText().toString();
            String overview = "OverView Unavailable";
            String link = "Link Unavailable";
            String imageUrl = "";
            int rate = parseInt(binding.rateEt.getText().toString());

            Movie movie = new Movie(name, userId, author, category, review,overview,link, imageUrl, rate);

            MovieDataApi.instance().getMovieDataByName(name, movieFromApi -> {
                try {
                    movie.setLink(movieFromApi.getString("youtubeTrailerVideoLink"));
                    movie.setOverview(movieFromApi.getString("overview"));
                    if (isImageSelected) {
                        binding.movieImg.setDrawingCacheEnabled(true);
                        binding.movieImg.buildDrawingCache();
                        Bitmap bitmap = ((BitmapDrawable) binding.movieImg.getDrawable()).getBitmap();
                        Model.instance().uploadImage(name, bitmap, url->{
                            if (url != null){
                                movie.setImageUrl(url);
                            }
                            Model.instance().addMovie(movie, (unused) -> {
                                dialog.dismiss();
                                Navigation.findNavController(view1).popBackStack();
                            });
                        });
                    } else {
                        Model.instance().addMovie(movie, (unused) -> {
                            dialog.dismiss();
                            Navigation.findNavController(view1).popBackStack();
                        });
                    }
                } catch (JSONException e) {                }
            });
        });
    }
}