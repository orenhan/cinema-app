package com.example.cinema_app;

import com.example.cinema_app.model.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MovieDataApi {
    private static final MovieDataApi instance = new MovieDataApi();
    private OkHttpClient client;

    private MovieDataApi() {
        client = new OkHttpClient();
    }

    public static MovieDataApi instance() {
        return instance;
    }

    public void getMovieDataByName(String name, Model.Listener<JSONObject> callback) {
        Request request = new Request.Builder()
                .url("https://streaming-availability.p.rapidapi.com/v2/search/title?title=" + name + "&country=us&type=movie&output_language=en")
                .get()
                .addHeader("X-RapidAPI-Key", "b0cc4ca2a5msh55c88d789b32810p1c8d0cjsn717eb92ec79d")
                .addHeader("X-RapidAPI-Host", "streaming-availability.p.rapidapi.com")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        JSONArray results = json.getJSONArray("result");
                        JSONObject bestResult = results.getJSONObject(0);
                        callback.onComplete(bestResult);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
