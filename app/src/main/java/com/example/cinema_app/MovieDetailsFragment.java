package com.example.cinema_app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.cinema_app.databinding.FragmentMovieDetailsBinding;
import com.example.cinema_app.model.FirebaseModel;
import com.example.cinema_app.model.Movie;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class MovieDetailsFragment extends Fragment {
    FragmentMovieDetailsBinding binding;

    public MovieDetailsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMovieDetailsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        setFields();

        return view;
    }

    private void setFields() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            Movie movie = (Movie) bundle.get("movie");
            binding.name.setText(movie.getName());
            binding.author.setText(movie.getAuthor());
            binding.category.setText(movie.getCategory());
            binding.review.setText(movie.getReview());
            binding.rate.setText(movie.getRate() + " *");
            binding.link.setText(movie.getLink());
            binding.overview.setText(movie.getOverview());
//            if (!Objects.equals(movie.getLink(), "")) {
//
//            }

            if (movie.getPhoto() != null) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(movie.getPhoto(), 0, movie.getPhoto().length);
                binding.movieImage.setImageBitmap(bitmap);
            }
            else if (!Objects.equals(movie.getImageUrl(), ""))
            {
                Picasso.get().load(movie.getImageUrl()).into(binding.movieImage);
            }

            FirebaseModel firebaseModel = new FirebaseModel();
            firebaseModel.getUserById(FirebaseAuth.getInstance().getCurrentUser().getUid(), user -> {
                binding.reviewerFullName.setText("Full Name:  " + user.getFullName());
                binding.reviewerPhoneNumber.setText("Phone Number:  " + user.getPhoneNumber());
                binding.reviewerMail.setText("Mail: " + user.getEmail());
            });
        }
    }
}