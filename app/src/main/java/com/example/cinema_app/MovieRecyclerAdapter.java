package com.example.cinema_app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cinema_app.databinding.MovieListRowBinding;
import com.example.cinema_app.model.Movie;
import com.example.cinema_app.model.FirebaseModel;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

class MovieViewHolder extends RecyclerView.ViewHolder {

    MovieListRowBinding binding;
    TextView categoryTv;
    TextView movieNameTv;
    TextView authorTv;
    ImageView movieImage;
    List<Movie> data;
    Button editButton;

    public MovieViewHolder(@NonNull View itemView, MovieRecyclerAdapter.OnItemClickListener listener, List<Movie> data) {
        super(itemView);
        this.binding = MovieListRowBinding.bind(itemView);;
        this.data = data;
        categoryTv = binding.category;
        movieNameTv = binding.movieName;
        authorTv = binding.author;
        movieImage = binding.movieImage;
        editButton = binding.editButton;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = getAdapterPosition();
                listener.onItemClick(pos);
            }
        });
    }

    public void bind(Movie movie, View view) {
        categoryTv.setText(movie.getCategory());
        movieNameTv.setText(movie.getName());
        authorTv.setText(movie.getAuthor());

        if (movie.getPhoto() != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(movie.getPhoto(), 0, movie.getPhoto().length);
            binding.movieImage.setImageBitmap(bitmap);
        } else if (!Objects.equals(movie.getImageUrl(), "")) {
            Picasso.get().load(movie.getImageUrl()).into(movieImage);
        }

        FirebaseModel firebaseModel = new FirebaseModel();
        firebaseModel.getUserById(FirebaseAuth.getInstance().getCurrentUser().getUid(), user -> {
            if(user.getId().equals(movie.getUserId())) {
                editButton.setVisibility(View.VISIBLE);
                NavDirections action1 = com.example.cinema_app.MyPostsFragmentDirections.actionMyPostsFragmentToAddMovieokFragment(movie, "Edit Movie");
                editButton.setOnClickListener(Navigation.createNavigateOnClickListener(action1));
            }
        });
    }
}

public class MovieRecyclerAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    OnItemClickListener listener;
    LayoutInflater inflater;
    List<Movie> data;

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    public void setData(List<Movie> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public MovieRecyclerAdapter(LayoutInflater inflater, List<Movie> data) {
        this.inflater = inflater;
        this.data = data;
    }

    void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.movie_list_row, parent,false);
        return new MovieViewHolder(view, listener, data);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        Movie movie = data.get(position);
        holder.bind(movie, holder.itemView);
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        return data.size();
    }
}

