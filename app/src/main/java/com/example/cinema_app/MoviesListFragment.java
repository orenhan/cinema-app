package com.example.cinema_app;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.cinema_app.databinding.FragmentMoviesListBinding;
import com.example.cinema_app.model.Model;
import com.example.cinema_app.model.Movie;

public class MoviesListFragment extends Fragment {
    FragmentMoviesListBinding binding;
    MovieRecyclerAdapter adapter;
    MoviesListFragmentViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMoviesListBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MovieRecyclerAdapter(getLayoutInflater(), viewModel.getData().getValue());
        binding.recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MovieRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Log.d("TAG", "Row was clicked " + pos);
                Movie movie = viewModel.getData().getValue().get(pos);
                MoviesListFragmentDirections.ActionMoviesListFragmentToMovieDetailsFragment action =
                        com.example.cinema_app.MoviesListFragmentDirections.actionMoviesListFragmentToMovieDetailsFragment(movie);
                Navigation.findNavController(view).navigate(action);
            }
        });

        binding.progressBar.setVisibility(View.GONE);

        viewModel.getData().observe(getViewLifecycleOwner(),list-> {
            adapter.setData(list);
        });

        Model.instance().EventMoviesListLoadingState.observe(getViewLifecycleOwner(), status-> {
            binding.swipeRefresh.setRefreshing(status == Model.LoadingState.LOADING);
        });

        binding.swipeRefresh.setOnRefreshListener(()-> {
            reloadData();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(MoviesListFragmentViewModel.class);
    }

    void reloadData() {
        Model.instance().refreshAllMovies();
    }
}