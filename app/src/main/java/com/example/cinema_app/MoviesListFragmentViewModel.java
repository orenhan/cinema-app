package com.example.cinema_app;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.cinema_app.model.Model;
import com.example.cinema_app.model.Movie;

import java.util.List;

public class MoviesListFragmentViewModel extends ViewModel {
    private LiveData<List<Movie>> data = Model.instance().getAllMovies();

    LiveData<List<Movie>> getData() {
        return data;
    }
}
