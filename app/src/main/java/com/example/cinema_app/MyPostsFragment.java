package com.example.cinema_app;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.cinema_app.databinding.FragmentMyPostsBinding;
import com.example.cinema_app.model.Movie;
import com.example.cinema_app.model.Model;

public class MyPostsFragment extends Fragment {
    FragmentMyPostsBinding binding;
    MovieRecyclerAdapter adapter;
    MyPostsFragmentViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMyPostsBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.myPostsRecyclerView.setHasFixedSize(true);
        binding.myPostsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new MovieRecyclerAdapter(getLayoutInflater(), viewModel.getData().getValue());
        binding.myPostsRecyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new MovieRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Log.d("TAG", "Row was clicked " + pos);
                Movie movie = viewModel.getData().getValue().get(pos);
                MyPostsFragmentDirections.ActionMyPostsFragmentToMovieDetailsFragment action = com.example.cinema_app.MyPostsFragmentDirections.actionMyPostsFragmentToMovieDetailsFragment(movie);
                Navigation.findNavController(view).navigate(action);
            }
        });

        View addButton = view.findViewById(R.id.addBtn);
        NavDirections action = com.example.cinema_app.MyPostsFragmentDirections.actionMyPostsFragmentToAddMovieokFragment(null, "Add Post");
        addButton.setOnClickListener(Navigation.createNavigateOnClickListener(action));

        binding.myPostsProgressBar.setVisibility(View.GONE);

        viewModel.getData().observe(getViewLifecycleOwner(),list->{
            adapter.setData(list);
        });

        Model.instance().EventMoviesListLoadingState.observe(getViewLifecycleOwner(), status->{
            binding.myPostsSwipeRefresh.setRefreshing(status == Model.LoadingState.LOADING);
        });

        binding.myPostsSwipeRefresh.setOnRefreshListener(()->{
            reloadData();
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(MyPostsFragmentViewModel.class);
    }

    void reloadData() {
        Model.instance().refreshAllMovies();
    }
}