package com.example.cinema_app;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.cinema_app.model.Movie;
import com.example.cinema_app.model.Model;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class MyPostsFragmentViewModel extends ViewModel {

    LiveData<List<Movie>> getData() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return Model.instance().getAllMoviesByUserId(user.getUid());
        }

        return null;
    }
}
