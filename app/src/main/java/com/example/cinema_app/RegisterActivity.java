package com.example.cinema_app;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cinema_app.databinding.ActivityRegisterBinding;
import com.example.cinema_app.model.Model;
import com.example.cinema_app.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText addressEditText;
    private EditText phoneNumberEditText;
    private EditText fullNameEditText;
    private ActivityRegisterBinding binding;
    private ActivityResultLauncher<Void> cameraLauncher;
    private ActivityResultLauncher<String> galleryLauncher;
    private Boolean isImageSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();

        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        this.emailEditText = binding.emailEt;
        this.passwordEditText  = binding.passwordEt;
        this.addressEditText  = binding.addressEt;
        this.phoneNumberEditText  = binding.phoneNumberEt;
        this.fullNameEditText   = binding.fullNameEt;

        setRegisterButtonActionListener();
        setLoginButtonActionListener();

        cameraLauncher = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), new ActivityResultCallback<Bitmap>() {
            @Override
            public void onActivityResult(Bitmap result) {
                if (result != null) {
                    binding.userImg.setImageBitmap(result);
                    isImageSelected = true;
                }
            }
        });

        galleryLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                if (result != null){
                    binding.userImg.setImageURI(result);
                    isImageSelected = true;
                }
            }
        });

        setCameraButtonActionListener();
        setGalleryButtonActionListener();
    }

    private void setLoginButtonActionListener() {
        Button loginBtn = binding.loginBtn;

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLoginActivity();
            }
        });
    }

    private void setRegisterButtonActionListener() {
        Button registerBtn = binding.RegisterBtn;

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
    }

    private void setGalleryButtonActionListener() {
        binding.galleryButton.setOnClickListener(view1->{
            galleryLauncher.launch("image/*");
        });
    }

    private void setCameraButtonActionListener() {
        binding.cameraButton.setOnClickListener(view1->{
            cameraLauncher.launch(null);
        });
    }

    private void createAccount () {
        mAuth.createUserWithEmailAndPassword(this.emailEditText.getText().toString(),
                        passwordEditText.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                                updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Intent intent = new Intent(this, MainActivity.class);
            User userToDb = new User(user.getUid(),
            this.fullNameEditText.getText().toString(),
            this.emailEditText.getText().toString(),
            this.passwordEditText.getText().toString(),
            this.phoneNumberEditText.getText().toString(),
            this.addressEditText.getText().toString());

            if (isImageSelected) {
                binding.userImg.setDrawingCacheEnabled(true);
                binding.userImg.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) binding.userImg.getDrawable()).getBitmap();
                Model.instance().uploadImage(user.getUid(), bitmap, url->{
                    if (url != null){
                        userToDb.setImageUrl(url);
                    }
                    Model.instance().addUser(userToDb, (unused) -> {
                        startActivity(intent);
                        finish();
                    });
                });
            } else {
                Model.instance().addUser(userToDb, (unused) -> {
                    startActivity(intent);
                    finish();
                });
            }
        }
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}