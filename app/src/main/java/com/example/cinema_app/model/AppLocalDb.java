package com.example.cinema_app.model;

import androidx.room.Room;

import com.example.cinema_app.MyApplication;

public class AppLocalDb {
    private AppLocalDb() {}

    static public AppLocalDbRepository getAppDb() {
        return Room.databaseBuilder(MyApplication.getMyContext(),
                        AppLocalDbRepository.class,
                        "movies.db")
                .fallbackToDestructiveMigration()
                .build();
    }
}

