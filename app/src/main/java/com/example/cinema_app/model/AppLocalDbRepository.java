package com.example.cinema_app.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Movie.class}, version = 6)
public abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract MovieDao movieDao();
}
