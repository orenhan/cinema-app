package com.example.cinema_app.model;

import android.graphics.Bitmap;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Model {
    final public MutableLiveData<LoadingState> EventMoviesListLoadingState = new MutableLiveData<LoadingState>(LoadingState.NOT_LOADING);
    private static final Model _instance = new Model();
    private LiveData<List<Movie>> movieList;
    private LiveData<List<Movie>> userMovieList;
    private Executor executor = Executors.newSingleThreadExecutor();
    private FirebaseModel firebaseModel = new FirebaseModel();
    AppLocalDbRepository localDb = AppLocalDb.getAppDb();

    private Model() {}

    public static Model instance() {
        return _instance;
    }

    public interface Listener<T> {
        void onComplete(T data);
    }

    public enum LoadingState {
        LOADING,
        NOT_LOADING
    }

    public LiveData<List<Movie>> getAllMovies() {
        if (movieList == null) {
            movieList = (LiveData<List<Movie>>) localDb.movieDao().getAll();
            refreshAllMovies();
        }
        return movieList;
    }

    public LiveData<List<Movie>> getAllMoviesByUserId(String userId) {
        if (userMovieList == null) {
            userMovieList = (LiveData<List<Movie>>) localDb.movieDao().getAllByUserId(userId);
        }

        return userMovieList;
    }

    public void refreshAllMovies(){
        EventMoviesListLoadingState.setValue(LoadingState.LOADING);
        Long localLastUpdate = Movie.getLocalLastUpdate();
        firebaseModel.getAllMoviesSince(localLastUpdate,list->{
            executor.execute(()->{
                Log.d("TAG", " firebase return : " + list.size());
                Long time = localLastUpdate;
                for(Movie movie: list){
                    movie.setPhoto(urlToByteArr(movie.getImageUrl()));
                    // insert new records into ROOM
                    localDb.movieDao().insertAll(movie);
                    if (time < movie.getLastUpdated()){
                        time = movie.getLastUpdated();
                    }
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Movie.setLocalLastUpdate(time);
                EventMoviesListLoadingState.postValue(LoadingState.NOT_LOADING);
            });
        });
    }

    public void addMovie(Movie movie, Listener<Void> listener){
        firebaseModel.addMovie(movie, (Void)->{
            refreshAllMovies();
            listener.onComplete(null);
        });
    }

    public void addUser(User user, Listener<Void> listener){
        firebaseModel.addUser(user, (Void)-> {
            listener.onComplete(null);
        });
    }

    public void uploadImage(String name, Bitmap bitmap,Listener<String> listener) {
        firebaseModel.uploadImage(name,bitmap,listener);
    }

    public byte[] urlToByteArr(String link) {
        byte[] imageBytes = null;
        try {
            URL url = new URL(link);
            InputStream inputStream = url.openStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int length;

            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }

            imageBytes = outputStream.toByteArray();
            inputStream.close();
            outputStream.close();

        } catch(Exception e) {
            System.out.println(e);
        } finally {
            return imageBytes;
        }
    }
}
