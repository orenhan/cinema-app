package com.example.cinema_app.model;

import static java.lang.Integer.parseInt;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.cinema_app.MyApplication;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Entity
public class Movie implements Serializable {
    private static final String NAME = "name";
    private static final String AUTHOR = "author";
    private static final String CATEGORY = "category";
    private static final String REVIEW = "review";
    private static final String OVERVIEW = "overview";
    private static final String LINK = "link";
    private static final String IMAGE_URL = "imageUrl";
    private static final String RATE = "rate";
    private static final String USER_ID = "userId";
    public static final String COLLECTION = "movies";
    static final String LAST_UPDATED = "lastUpdated";
    static final String LOCAL_LAST_UPDATED = "movies_local_last_update";

    @PrimaryKey
    @NonNull
    private String name;
    private String userId;
    private String author;
    private String category;
    private String review;
    private String overview;
    private String link;
    private String imageUrl;
    private int rate;
    public Long lastUpdated;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    public byte[] photo;

    public Movie() {}

    public Movie(@NonNull String name, String userId, String author, String category, String review,String overview, String link, String imageUrl, int rate) {
        this.name = name;
        this.userId = userId;
        this.author = author;
        this.category = category;
        this.review = review;
        this.overview = overview;
        this.link = link;
        this.imageUrl = imageUrl;
        this.rate = rate;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getReview() {
        return this.review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getOverview() {
        return this.overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public static Movie fromJson(Map<String, Object> json) {
        String name = (String)json.get(NAME);
        String userId = (String)json.get(USER_ID);
        String author = (String)json.get(AUTHOR);
        String category = (String)json.get(CATEGORY);
        String review = (String)json.get(REVIEW);
        String overview = (String)json.get(OVERVIEW);
        String link = (String)json.get(LINK);
        String imageUrl = (String)json.get(IMAGE_URL);
        int rate = parseInt(json.get(RATE).toString());

        Movie movie = new Movie(name, userId, author, category, review,overview, link, imageUrl , rate);

        try {
            Timestamp time = (Timestamp) json.get(LAST_UPDATED);
            movie.setLastUpdated(time.getSeconds());
        } catch(Exception e) {}

        return movie;
    }

    public static Long getLocalLastUpdate() {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        return sharedPref.getLong(LOCAL_LAST_UPDATED, 0);
    }

    public static void setLocalLastUpdate(Long time) {
        SharedPreferences sharedPref = MyApplication.getMyContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(LOCAL_LAST_UPDATED,time);
        editor.commit();
    }

    public Map<String,Object> toJson() {
        Map<String, Object> json = new HashMap<>();

        json.put(NAME, getName());
        json.put(USER_ID, getUserId());
        json.put(AUTHOR, getAuthor());
        json.put(CATEGORY, getCategory());
        json.put(REVIEW, getReview());
        json.put(OVERVIEW, getOverview());
        json.put(LINK, getLink());
        json.put(IMAGE_URL, getImageUrl());
        json.put(RATE, getRate());
        json.put(LAST_UPDATED, FieldValue.serverTimestamp());

        return json;
    }
}
