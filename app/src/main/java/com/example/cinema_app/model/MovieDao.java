package com.example.cinema_app.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import java.util.List;

@Dao
public interface MovieDao {
    @Query("select * from Movie")
    LiveData<List<Movie>> getAll();

    @Query("select * from Movie where userId = :userId")
    LiveData<List<Movie>> getAllByUserId(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Movie... movies);
}

