package com.example.cinema_app.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.HashMap;
import java.util.Map;

@Entity
public class User {
    public static final String COLLECTION = "users";
    private static final String ID = "id";
    private static final String FULL_NAME = "fullName";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String ADDRESS = "address";
    private static final String IMAGE_URL = "imageUrl";

    @PrimaryKey
    @NonNull
    private String id;
    private String fullName;
    private String email;
    private String password;
    private String phoneNumber;
    private String address;
    private String imageUrl;

    public User() {}

    public User(@NonNull String id, String fullName, String email, String password, String phoneNumber, String address, String imageUrl) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.imageUrl = imageUrl;
    }

    public User(@NonNull String id, String fullName, String password, String phoneNumber, String address, String imageUrl) {
        this.id = id;
        this.fullName = fullName;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public static User fromJson(Map<String, Object> json) {
        String id = (String)json.get(ID);
        String fullName = (String)json.get(FULL_NAME);
        String username = (String)json.get(EMAIL);
        String password = (String)json.get(PASSWORD);
        String phoneNumber = (String)json.get(PHONE_NUMBER);
        String address = (String)json.get(ADDRESS);

        User user = new User(id, fullName, username, password, phoneNumber, address);

        return user;
    }

    public Map<String,Object> toJson() {
        Map<String, Object> json = new HashMap<>();

        json.put(ID, getId());
        json.put(FULL_NAME, getFullName());
        json.put(EMAIL, getEmail());
        json.put(PASSWORD, getPassword());
        json.put(PHONE_NUMBER, getPhoneNumber());
        json.put(ADDRESS, getAddress());
        json.put(IMAGE_URL, getImageUrl());

        return json;
    }
}
